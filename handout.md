---
author: Manuela Salvucci
title: GIT workshop handout
date: 2019-11-06
email: manuelasalvucci@rcsi.ie
---

# Installation

1. Go to <https://git-scm.com/>
2. Download executable in suggested directory
3. Install by following step-by-step instructions and accepts default settings

# Verify installation and setup
1. Verify installation completed successfully
   ```bash
   git --version
   ```
2. Setup name to be used in the GIT history
   ```bash
   git config --global user.name "Manuela Salvucci"
   ```
3. Setup email-address to be used in the GIT history
   ```bash
   git config --global user.email "manuelasalvucci@rcsi.ie"
   ```
4. Set GIT default editor
   ```bash
   git config --global core.editor "notepad"
   ```

\clearpage

# Cheat-sheet

## Version Control - Vocabulary
- **repository**: Folder (and sub-folders) under version control
- **commit**: A set of changes with a commit message
- **commit sha**: A unique id that identifies a commit
- **revision**: A snapshot of the repository. Identified by the sha of the last commit included in the snapshot
- **branch**: Independent line of development within the repository
- **master**: Conventionally name of the main branch

## Commands

<Text inside angle brackets>
Placeholder for which you must supply a value

- Get help
  ```bash
  git help
  git help <verb>
  git <verb> --help
  ```
- Initialize repository
  ```bash
  git init
  ```
- Check status of GIT repository
  ```bash
  git status
  ```
- Prepare a file to be added to GIT repository. 
  ```bash
  git add <directory or filename(s)>
  ```
- Commit a set of changes to the repository
  ```bash
  git commit -m <message> <filenames>
  ```
- See the history of the GIT repository
  ```bash
  git log
  ```
- Built in graphical history viewer
  ```bash
  gitk
  ```
- Compare the current files to last commit or compare two revisions of the repository
  ```bash
  git diff
  git diff <commit1 sha> <commit2 sha>
  ```
- Undo a previous change
  ```bash
  git revert <commit sha>
  ```
- Download a repository from another computer/cloud
  ```bash
  git clone <url>
  ```
- Synchronize recent changes from another computer/cloud
  ```bash
  git pull
  ```
- Synchronize recent changes to another computer/cloud
   ```bash
   git push
   ```

\clearpage

# Exercise 1

1. Create a folder named "christmas_repo"
2. Open GIT bash, verify the installation and configure GIT
3. Initialize a GIT repository in the folder

4. Create a text file ("wish_list.md") with 3 gifts you wish to receive for Christmas 
5. Add and commit the wish list file to GIT

6. Edit the wish list file, and add 2 more presents
7. Use GIT to check the difference between the current and previous version
8. Commit the updated file

9. Create a file ("recipients.md") with a list of people you plan to buy gifts for
10. Check the status of the GIT repository
11. Add and commit the new file to GIT

12. Create a file ("past_gifts.md") with a list of what gifts you gave last year
13. Maybe you remembered a few more people you would like to give gifts to. Add them to "recipients.md"
14. Add the new file and commit "past_gifts.md" and "recipients.md" to GIT

15. Look at the GIT history
16. Revert the change that added more presents to the wish list in step 5

17. Play around with doing more changes and commits

\clearpage

# Exercise 2 

1. Create a BitBucket account
    1. Go to <https://bitbucket.org/>
    2. Create an account
    3. Finalize account (requires you to access your email for verification)

2. Push your "christmas_repo" from exercise 1 to BitBucket
    1. Create "christmas_repo" repository on BitBucket
    2. Connect your local repository to BitBucket
       ```bash
       git config --global http.sslVerify false
       ```
       ```bash
       git remote add origin https://manuela_s@bitbucket.org/manuela_s/christmas_repo.git
       ```
       (replace both occurrences of "manuela_s" with the username you chose in step 2)
       ```bash
       git push -u origin master
       ```
    3. Inspect history and file content on BitBucket

3. Add more past_gifts and push to BitBucket
    1. In your local repository (your computer), modify "past_gifts.md" with gifts you gave 2 years ago
    2. Commit the change to (local) repository
    3. Push the change to BitBucket
    4. Go to BitBucket and "annotate" the history of the "past_gifts.md" file

4. Update recipients through the BitBucket web-interface, and pull to your local machine.
    1. Use the BitBucket web-interface to edit "recipients.md" to include 2 extra people
    2. Go to your local repository (your computer) and pull the change
    3. Verify that "recipients.md" is updated

5. Collaborate in pairs
    1. Make your repository accessible to the other person by changing the BitBucket settings for your repository:
    
       Settings -> User and group access
       - Add other person
       - Change permission to Write  
    2. Clone the other person repository
       - open the GIT bash in a new folder on your computer
       - run git clone 
         - hint: find the full command on BitBucket --> Source --> clone (HTTPS)
    3. Add yourself to their "recipients.md" file
    4. Commit change
    5. Push to BitBucket
    6. Wait for the present to come at Christmas!