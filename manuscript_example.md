```markdown
---
title: 'HCP: A Matlab package to create beautiful heatmaps with richly annotated covariates'
authors:
 - name: Manuela Salvucci
   orcid: 0000-0001-9941-4307
   affiliation: 1
 - name: Jochen H. M. Prehn
   orcid: 0000-0003-3479-7794
   affiliation: 1
affiliations:
 - name: Centre for Systems Medicine, Department of Physiology and Medical Physics, Royal College of Surgeons in Ireland, Dublin, Ireland
   index: 1
date: 20 January 2019
bibliography: paper.bib
---

# Summary
A heatmap is a graphical technique that maps 2-dimensional matrices of numerical values to colors to provide an immediate
and intuitive visualization of the underlying patterns [@Eisen1998]. Heatmaps are often used in conjunction with cluster
analysis to re-order observations and/or features by similarity and thus, rendering common and distinct patterns more apparent.
When generating these visualizations, it is often of interest to interpret the underlying patterns in the context of other
data sources. In the field of bioinformatics, heatmaps are frequently used to visualize high-throughput and high-dimensional
datasets, such as those derived from profiling biological samples with *-omic* technologies (whole genome sequencing,
transcriptomics and proteomics). Often, biological samples (for example, patient tumour samples) are characterized at
multiple *-omic* level and it is of interest to contrast and compare patterns captured at the different molecular layers
along with their associations with other observable features (covariates). The concurrent display of continuous or
categorical covariates enriches the visualization with additional information such as group membership.
```
