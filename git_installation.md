## Git installation  { .example data-transition=“fade”}

::: columns
:::: {.column width="30%"}
1. Go to <https://git-scm.com/>
2. Download executable in suggested directory
3. Install by following step-by-step instructions and accepts default settings
4. Verify installation completed successfully
::::

:::: {.column width="70%"}
![**$caption**](assets/installation_$screenshot.png){ .screenshot }
::::
:::
