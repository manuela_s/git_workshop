---
author: Manuela Salvucci
title: GIT workshop solutions
date: 2019-11-06
email: manuelasalvucci@rcsi.ie
---
# Exercise 1 - Solution

1. Create a folder named "christmas_repo"

2. Open GIT bash, verify the installation and configure GIT
   ```bash
   git config --global user.name "Manuela Salvucci"
   ```
   ```bash
   git config --global user.email "manuelasalvucci@rcsi.ie"
   ```
   ```bash
   git config --global core.editor "notepad"
   ```

3. Initialize a git repository in the folder
   ```bash
   git init
   ```

4. Create a text file ("wish_list.md") with 3 gifts you wish to receive for Christmas
5. Add and commit the wish list file to GIT
   ```bash
   git add wish_list.md
   ```
   ```bash
   git commit -m "Create wish list" wish_list.md
   ```

6. Edit the wish list file, and add 2 more presents
7. Use GIT to check the difference between the current and previous version
   ```bash
   git diff wish_list.md
   ```
8. Commit the updated file
   ```bash
   git commit -m "Add 2 more gifts to the wish list" wish_list.md
   ```

9. Create a file ("recipients.md") with a list of people you plan to buy gifts for
10. Check the status of the GIT repository
    ```bash
    git status
    ```
11. Add and commit the new file to GIT
    ```bash
    git add recipients.md
    ```
    ```bash
    git commit -m "Create a list of gift recipients" recipients.md
    ```

12. Create a file ("past_gifts.md") with a list of what gifts you gave last year
13. Maybe you remembered a few more people you would like to give gifts to. Add them to "recipients.md"
14. Add the new file and commit "past_gifts.md" and "recipients.md" to GIT
    ```bash
    git add past_gifts.md
    ```
    ```bash
    git commit -m "Create a list of gifts given in the past and update recipients list" past_gifts.md recipients.md
    ```

15. Look at the GIT history
    ```bash
    git log --oneline
    ```
16. Revert the change that added more presents to the wish list in step 5
    ```bash
    git revert <SHA for "Add 2 more gifts to the wish list" from step 15>
    ```

17. Play around with doing more changes and commits