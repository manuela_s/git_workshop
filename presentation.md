---
author: Manuela Salvucci
pagetitle: GIT workshop
date: 2019-11-06
email: manuelasalvucci@rcsi.ie
slideNumber: true
---

# GIT workshop

## GIT workshop

![](assets/git_logo.png)

Manuela Salvucci

<manuelasalvucci@rcsi.ie>

2019-11-06

![](assets/rcsi_logo.png){ height=105px }

## Outline

- What is version control?
- Why bother with "formal" version control?
- How to install and get started with GIT
- Use GIT core features
- Review files history, revert/amend changes
- Collaborate online with others with BitBucket, GitHub or GitLab
- Hands-on examples

# Version control

## What is version control?

> "Version control is a system that records changes to a file or set of files over time so that you can recall
> specific versions later."

<small>*(Pro Git, Scott Chacon and Ben Straub, 2014)*</small>

![<small>From <https://wac-cdn.atlassian.com/dam/jcr:34e935dd-3108-40ef-bb3d-9ed01d977d6d/hero.svg?cdnVersion=659></small>](assets/what_is_version_control.png){ height=8cm }

## Without version control... a way too familiar picture
![<small>From <http://phdcomics.com/comics/archive.php?comicid=1531></small>](assets/phd101212s.gif){ .full }

## Without version control... "informal" versioning

- **None**
- **Named files**:
    - OK:
        - manuscript_my_draft.docx
        - manuscript_my_draft_with_coauthor_comments.docx
        - ...
    - Better:
        - manuscript_draft_v01.docx
        - manuscript_draft_v02.docx
        - ...
- **Named zip-files**:
    - manuscript_drafts.zip
    - manuscript_cell_submission.zip
    - manuscript_pnas_submission.zip
    - manuscript_pnas_revisions.zip
    - manuscript_pnas_proofs.zip
- **Sync online services** (Microsoft/Dropbox/Google/Overleaf/Sharelatex)

## Without version control... challenges
::: columns
:::: {.column width="70%"}
- Time consuming
- Error prone
- Requires self-discipline (save everything, good file names, sticking to a routine, ...)
- Relationship between changes in multiple files is lost
- Information about what, when and why something changed is lost?
    - How would you go about finding out when the p-value for Figure 2.A
      got set to the (wrong) value?
- Non-linear history (parallel versions)
- Disk space
::::

:::: {.column width="30%"}
![<small>From <https://dynamicbusiness.com.au/wp-content/uploads/2012/09/></small>](assets/puzzle-pieces.jpg)
::::
:::

## Why bother with "formal" version control?

- We are too busy to use inefficient, manual, error-prone versioning
- Research is increasingly collaborative:
    - we need a better way to document the rational behind data cleaning, analysis steps, generation of figures, write-ups... 
    - we need a better way to "merge" inputs and feedback to the project from collaborators
    - often your future self is the collaborator (and you don't reply to emails...)
- We do research anywhere:
    - on our workstation at work
    - on the laptop at home/bus/conference
    - on a dedicated facility workstation
    - ...
- Projects are always evolving and never "really" finished

## What can Version Control Systems do for my research?
- Version Control Systems are software that keep track of your files and their full history
- Project files and "history" in the form of "snapshots"/"checkpoints" are organized in a folder
- Explicitly indicate what file(s) and what change(s) to store with a named snapshot (include why the changes were made)
- Can "go back in time" and see/use files how they look at a specific snapshot
- Can see what changed between snapshots, and in what snapshot content was first introduced
- Can "experiment" by having "organized parallel versions" of files
- Synchronize different copies of the project between different computers/collaborators

## Version Control Systems - Vocabulary
- Version Control Systems are software that keep track of your files and their full history
- Project files and "history" in the form of "snapshots"/"checkpoints" are organized in a folder -> **repository**
- Explicitly indicate what file(s) and what change(s) to store with a named snapshot (include why the changes were made) -> **commit** or **revision**
- Can "go back in time" or "jump forward" and see/use files how they look at a specific snapshot -> **checkout** or **revert**
- Can see what changed between snapshots, and in what snapshot content was first introduced -> **diff**, **annotate** and **blame**
- Can "experiment" by having "organized parallel versions" of files -> **branch**
- Synchronize different copies of the project between different computers/collaborators -> **push** & **pull**

## What type of files can I track with version control?
- All types of files can be tracked with version control (but big files may require special care)
- **Version control is most useful for plain "text"-files** (txt, md, tex, csv, .py, .R, .m, html, ....) where differences between versions can be "easily" visualized and multiple changes can be merged/combined automatically
- Version control works also for binary files (docx, xlsx, etc.), but it would only tell us if there is a change, but not visualize the change and the version control system will not be able to merge changes automatically

## What version control systems are available?

::: columns

:::: column
- **GIT**, PerForce, Mercurial, Subversion (SVN), Bazaar, Concurrent Versions System (CVS), Monotone, ....
- We will focus on GIT in this workshop
::::

:::: column
![<small>From <https://twitter.com/rhodecode></small>](assets/screenshot_version_control_software_popularity.png){ .screenshot }
::::

:::


## Centralised vs. distributed version control system

::: columns

:::: {.column width="50%"}
- In the centralized setup, there is a single (central) copy of the project and each user will apply changes to the central copy
- In the distributed setup, each user has their own (full) copy of the project (a clone)

- SVN, PerForce, CVS are examples of centralised version control system
- **GIT** and Mercurial are examples of **distributed version control system**

::::

:::: {.column width="50%"}
![<small>From <https://github.com/AnnieCannons/ac-terminal-and-git/blob/gh-pages/images/versioncontrol></small>](assets/centralised_vs_distributed_version_control.png)
::::
:::

# GIT

## GIT
![<small>From <https://git-scm.com/></small>](assets/git_logo.png)

- Popular version control software:
    - Distributed system
    - Free and Open Source
    - Available for Windows, Linux and Mac
    - A lot of support, infrastructure and tools available to interface with GIT:
        - graphical user interfaces (GUIs)
        - seamless integration with Integrated Development Environments (IDEs) for R, MATLAB, Python, ...
        - cloud services (BitBucket, GitHub, GitLab)
- Developed by the Linus Torsvalds in 2005 to manage the development of Linux and maintained by Junio Hamano

## Tracking large files with GIT

> "Git Large File Storage (LFS) replaces large files such as audio samples, videos, datasets, and graphics with text pointers inside Git, while storing the file contents on a remote server like GitHub.com or GitHub Enterprise"

<small><https://git-lfs.github.com/></small>

::: columns
:::: {.column width="50%"}
- Drop-in replacement for "normal" GIT -> **git lfs add** vs. **git add**
- Files are stored "externally", so that GIT operations can run seamlessly and fast
- Good solution for "large" files (100 MB - 2 GB)
::::

:::: {.column width="50%"}
![<small>From <https://git-lfs.github.com/></small>](assets/git_lfs.png){ height=4cm }
::::
:::

- Alternatives:
    - [git-annex](https://git-annex.branchable.com/)
    - Do not version control large file
        - set permission to Read Only
        - version control metadata instead
    - ...

## Exclude files from tracking

- **.gitignore**: "special" file to list files and folders to intentionally not track
- Prevents files/folders from showing when running *git status* -> less clutter
- Can also be added by running *git add -f* (short for force)

- **Rationale**: not all files need to be version controlled
    - figures, tables, manuscript pdf generated by running code -> version control the raw data and the code to generate outputs instead
    - temporary files, compiled outputs, ...
    
- Checkout [https://www.gitignore.io/](https://www.gitignore.io/) to help identify files to ignore

# Installation

!include git_installation.md screenshot '01' highlight 'Go.+' caption 'Go to <https://git-scm.com/>'

!include git_installation.md screenshot '02' highlight 'Download.+' caption 'Select to download latest stable GIT release for Windows'

!include git_installation.md screenshot '03' highlight 'Download.+' caption 'Wait for executable to download'

!include git_installation.md screenshot '04' highlight 'Download.+' caption 'Save executable in suggested folder'

!include git_installation.md screenshot '05' highlight 'Install.+' caption 'Click on executable to start installation process'

!include git_installation.md screenshot '06' highlight 'Install.+' caption 'Select *Install anyway*'

!include git_installation.md screenshot '07' highlight 'Install.+' caption 'Select *Yes*'

!include git_installation.md screenshot '08' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '09' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '10' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '11' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '12' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '13' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '14' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '15' highlight 'Install.+' caption 'Accept default settings by clicking on *Next*'

!include git_installation.md screenshot '16' highlight 'Install.+' caption 'Accept default settings by clicking on *Install*'

!include git_installation.md screenshot '17' highlight 'Install.+' caption 'Monitor installation progress'

!include git_installation.md screenshot '18' highlight "Install.+" caption 'Select *Launch Git Bash*, Unselect *View Release Notes* and click on *Finish*'

!include git_installation.md screenshot '19' highlight 'Verify.+' caption 'Verify installation completed successfully'


## Now you install GIT on your computer (5-10 min)

1. Go to <https://git-scm.com/>
2. Download executable in suggested directory
3. Install by following step-by-step instructions and accepts default settings

![**Signal once installation progress has started**](assets/installation_17.png){ height=6cm }

# Demo

## Demo

To demonstrate we are going to go through an example for writing a manuscript.

- We will track the history of our manuscript and accompanying files in git
- We will use git to see the history of our files and to undo a mistake
- We will use git to synchronize the files between multiple computers and to collaborate with other authors

## Writing papers with Markdown
> "Markdown is a lightweight markup language with plain text formatting syntax"

<small>(Wikipedia)</small>

- Markdown text (.md extension) can be converted to other formats (.docx, .pdf, .html) with [Pandoc](https://pandoc.org/MANUAL.html)
- References can also be stored in plain text files (.bib)
- Learn more about markdown [here](https://www.markdownguide.org/)
- [Try it online](https://dillinger.io/)

## Writing papers with Markdown - example

::: columns

:::: {.column width="50%"}

Markdown manuscript

!include manuscript_example.md
::::

:::: {.column width="50%"}
Markdown pdf

![](assets/screenshot_pdf_hcp_paper.png)
::::

:::


## Getting started

Two main approaches to get a git repository:

- start a repository from scratch -> **git init**
- start by cloning an existing repository -> **git clone**

## Practical example

!include practical_example.md screenshot 'make_directory' highlight 'Make a project folder' caption 'Make a project folder'

!include practical_example.md screenshot 'make_directory2' highlight 'Make a project folder' caption 'Name it "demo"'

!include practical_example.md screenshot 'open_git_bash' highlight 'Start a git.+' caption 'Open GIT bash'

!include practical_example.md screenshot 'git_bash' highlight 'Start a git.+' caption 'GIT bash'

!include practical_example.md screenshot 'git_config' highlight 'Configure git' caption 'Configure GIT'

!include practical_example.md screenshot 'git_init' highlight 'Initialize repository' caption 'Initialize repository'

!include practical_example.md screenshot 'empty_folder' highlight 'Initialize repository' caption 'The folder still looks empty after git init. There is a hidden .git directory that you can normally not see'

!include practical_example.md screenshot 'git_folder' highlight 'Initialize repository' caption 'If you explicitly open the .git subdirectory, you can see a lot of files internal to GIT. You do not need to directly interact with these files (and do not delete them)'

!include practical_example.md screenshot 'new_text_document' highlight 'Make edits' caption 'Create a new text document for a manuscript we are writing'

!include practical_example.md screenshot 'new_text_document_md' highlight 'Make edits' caption 'Rename the file to manuscript.md to indicate that the file is formatted with markdown'

!include practical_example.md screenshot 'initial_manuscript' highlight 'Make edits' caption 'First manuscript draft'

!include practical_example.md screenshot 'git_status_01' highlight 'git status' caption 'We can use git status command to see what the repository status is'

!include practical_example.md screenshot 'git_add_manuscript' highlight 'git add' caption 'To prepare a new file to be added to the repository, we use git add. If we re-run git status we now see that the file is staged'

!include practical_example.md screenshot 'git_initial_commit' highlight 'git commit' caption 'To store changes in the repository, we use git commit. We specify a commit message after -m to record what we did'

## Anatomy of a commit
- Includes:
    - what changed compared to the previous commit (**snapshot**)
        - which files are affected by changes and how
    - rationale for the change (**commit message**)
    - timestamp 
    - **"name"**: unique identifier represented by [SHA-1 hashes](https://en.wikipedia.org/wiki/SHA-1)
        - for example: 4fc82ba7bb3f3a3de8ac57f16b6a926a7e60a21e
        - first 6 digits are *typically* sufficient to describe a commit -> shorthand version 4fc82ba
    - **"parent" commit** (reference to previous snapshot)
        - first commit is special (has no parent)
        - last commit is special (it is called HEAD)
- The full series of commits makes up the whole project

## Guidelines on commits... size matter

- Commit small units of changes and commit often
- A good unit of change is a small, self-contained, working change
    - **GOOD:** data.csv, process_data_figure1.py, make_figure1.py
    - **BAD:** 1 commit with a day worth of work (on multiple fronts)
- **Rule of thumb:** commit together what you would need to undo if you later want to disregard this change

## Guidelines on commits... message

::: columns

:::: {.column width="60%"}
- Write good commit messages:
    - **GOOD:** Update ReadMe to include 'how-to-install' section. Fixes issue ##1
    - **BAD:** Major fixup
    - which of the 2 messages above would you rather read the evening before a deadline?
- **A perfect commit message summarises the what and why of the change, not the how (can be seen from the diffs)**
- Other advice include:
    - keep the message subject coincise (<50 words) -> log looks cleaner
    - add additional details (if needed) after a blank line and wrap at 72 characters -> readability
    - use imperative verb (*Add* vs. *Added*) -> if change get reverted, message reads better (Revert *Add* ...)
    - use commit.template
    
- [Examples of how (not to) write commit messages](http://whatthecommit.com/)
- [More tips on writing good commit messages](https://github.com/erlang/otp/wiki/writing-good-commit-messages)
::::

:::: {.column width="40%"}
![](assets/xkcd_commit_message_cartoon.png)
<small>From <https://xkcd.com/1296/></small>
::::

:::

!include practical_example.md screenshot 'git_status_02' highlight 'git status' caption 'We use GIT status to check that there are no outstanding changes'

!include practical_example.md screenshot 'manuscript_02' highlight 'Make edits' caption 'Let us do some more work on the manuscript. We need to add more details for materials and methods and add a section for references'

!include practical_example.md screenshot 'commit_02' highlight 'git commit' caption 'Once we have finished with our change, we use git commit to add the new version of the file to the GIT repository'

!include practical_example.md screenshot 'manuscript_03' highlight 'Make edits' caption 'We changed our mind, and we will use mutation data instead of RNASeq. Let us update the materials and methods'

!include practical_example.md screenshot 'commit_03' highlight 'git commit' caption 'Commit the change as before'

!include practical_example.md screenshot 'manuscript_04' highlight 'Make edits' caption 'Add figures and tables to our manuscript'

!include practical_example.md screenshot 'git_diff' highlight 'git diff' caption 'We can use the git diff command to see how our current files are different from the last one checked into the repository'

!include practical_example.md screenshot 'git_status_04' highlight 'git status' caption 'If we create a figures directory with some image files and run git diff we see that this directory is untracked by GIT'

!include practical_example.md screenshot 'git_status_05' highlight 'git add' caption 'We add the whole directory with git add and rerun git status. Now it lists the files as new instead'

!include practical_example.md screenshot 'commit_04' highlight 'git commit' caption 'To commit the files, we use git commit -a instead of listing them. -a means all, and will commit all files that we have added or modified'

!include practical_example.md screenshot 'git_log' highlight 'git log' caption 'The git log command can show a history from the repository. Last change on top. --online gives a more compact representation'

!include practical_example.md screenshot 'git_diff_02' highlight 'git diff' caption 'Git diff can also be used to show the difference between two revisions in the history. We need to specify the two commit identifiers'

!include practical_example.md screenshot 'git_revert_01' highlight 'git revert' caption 'Actually, we changed our mind again, and want to use RNASeq. Let us revert the previous change'

!include practical_example.md screenshot 'git_revert_02' highlight 'git revert' caption 'GIT will ask us for a commit message for the revert. The default message is fine'

!include practical_example.md screenshot 'git_revert_03' highlight 'git revert' caption 'GIT confirms the change, like a normal commit'

!include practical_example.md screenshot 'git_log_02' highlight 'git revert' caption 'The history captures the revert'

!include practical_example.md screenshot 'manuscript_after_revert' highlight 'git revert' caption 'Note that we did not just go back to a previous revision. We selectively undid the RNASeq->mutation change, but we still have the figures and tables, which was added afterwards. GIT has automatically merged our changes together'

## Other useful GIT commands

- **git rm FILENAME**: delete tracked file
- **git mv FILENAME1 FILENAME2**: rename file from FILENAME1 to FILENAME2
- **git log --follow**: inspec t log (even with renaming)

## I am working on it...

- **git add -p FILENAME**: add portions of changes you made to a file
    - preserve other changes, but they will not be captured in this commit
    - useful when you set out to make some changes, but you could not help fixing (other) unrelated stuff
- **git squash**: pool related commits in a meta-commit
- **git stash**: stash away *work in progress* which is in a state that is too preliminary to be committed and get back to it later
    - **git stash list**
    - **git stash pop**
    - **git stash drop**

## Ops, I did not mean to do that.... Let's pretend it never happened

- **git commit --amend**: by far the most used command
    - useful when you forgot to add a file before committing or you would like to change commit message
- **git revert SHA**: revert changes applied by SHA by creating a new commit
- **git checkout FILENAME**: undo (uncommited) changes to FILENAME
- **git checkout SHA**: checkout a snapshot where all was good
- **git reset**: undo changes, degree of annihilation depends on flags (*--soft* vs. *--hard*), be careful

## Ops, something went terribly terribly wrong, at some point in the past

- **git show <SHA>**: inspect (suspicious) commit
- **git blame**: when and who changed/broke this?
- **git bisect**: run binary search to identify when problem was introduced
    - extremelly useful command, a life-saver
    - requires *knowing* what *right* vs. *wrong* means (unit tests, ground truth, ...)

## GIT SubModules... russian dolls repositories
> "It often happens that while working on one project, you need to use another project from within it. Perhaps it’s a library that a third party developed or that you’re developing separately and using in multiple parent projects. A common issue arises in these scenarios: you want to be able to treat the two projects as separate yet still be able to use one from within the other."

<small>*<https://git-scm.com/book/en/v2/Git-Tools-Submodules>*</small>

![](assets/submodule.png){ height=8cm }

# Exercise 1

## Exercise 1 (20 min)

1. Create a folder named "christmas_repo"
2. Open GIT bash, verify the installation and configure GIT
3. Initialize a GIT repository in the folder
4. Create a text file ("wish_list.md") with 3 gifts you wish to receive for Christmas 
5. Add and commit the wish list file to GIT
6. Edit the wish list file, and add 2 more presents
7. Use GIT to check the difference between the current and previous version
8. Commit the updated file
9. Create a file ("recipients.md") with a list of people you plan to buy gifts for
10. Check the status of the GIT repository
11. Add and commit the new file to GIT
12. Create a file ("past_gifts.md") with a list of what gifts you gave last year
13. Maybe you remembered a few more people you would like to give gifts to. Add them to "recipients.md"
14. Add the new file and commit "past_gifts.md" and "recipients.md" to GIT
15. Look at the GIT history
16. Revert the change that added more presents to the wish list in step 5
17. Play around with doing more changes and commits

# GIT support tools

## GIT support tools
- Graphical user interface (GUIs)
- Integration with software Integrated Development Environments (IDEs) for R, MATLAB, Python, ...
- Cloud services (BitBucket, GitHub, GitLab)

## GIT graphical user interface (GUI)
![From <https://git-scm.com/downloads/guis>](assets/git_tools_gui.png){ .full }

## GIT graphical user interface (GUI)
![](assets/git_tools_gitk_01.png){ .full }

## GIT graphical user interface (GUI)
![](assets/git_tools_gitk_02.png){ .full }

## GIT integration with software Integrated Development Environments (IDE)

::: columns

:::: {.column width="33%"}
RStudio (R IDE)
![](assets/git_tools_rstudio.png)
::::

:::: {.column width="33%"}
MATLAB (MATLAB IDE)
![](assets/git_tools_matlab.png)
::::

:::: {.column width="33%"}
PyCharm (Python IDE)
![](assets/git_tools_pycharm.png)
::::

:::

## Cloud services that support GIT... "social" coding

- Servers that can host a copy of your repository
- Useful as a backup
- Can make synchronization and collaboration easier
- Free plans available
- Most popular alternatives:

::: columns

:::: {.column width="33%"}
[![](assets/git_tools_github.png){ height=1.5cm }](https://github.com/)
::::

:::: {.column width="33%"}
[![](assets/git_tools_bitbucket.png){ height=1.5cm }](https://bitbucket.org/)
::::

:::: {.column width="33%"}
[![](assets/git_tools_gitlab.png){ height=1.5cm }](https://about.gitlab.com/)
::::

:::

- Other alternatives include Crucible, AWS CodeCommit, CodeCommit, ....

## Cloud services that support GIT... "social" coding

**Comparison of key features in free plans from GitHub, BitBucket and GitLab**
![](assets/github_vs_bitbucket_vs_gitlab.png)

- Similar products, select the one that suits best your needs

# Bitbucket

!include bitbucket_example.md screenshot 'create_01' highlight 'Create account.+' caption 'Go to the BitBucket website and click *Get Started*'

!include bitbucket_example.md screenshot 'create_02' highlight 'Create account.+' caption 'Follow instruction by filling in required info'

!include bitbucket_example.md screenshot 'create_03' highlight 'Create account.+' caption 'Follow instruction by filling in required info'

!include bitbucket_example.md screenshot 'create_04' highlight 'Create account.+' caption 'Verify email'

!include bitbucket_example.md screenshot 'create_06' highlight 'Create account.+' caption 'Log in with your credential'

!include bitbucket_example.md screenshot 'create_07' highlight 'Create account.+' caption 'Log in with your credential'

!include bitbucket_example.md screenshot 'create_08' highlight 'Create account.+' caption 'Choose your username'

!include bitbucket_example.md screenshot 'create_09' highlight 'Create account.+' caption 'Finalize setup'

!include bitbucket_example.md screenshot 'create_10' highlight 'Create account.+' caption 'Complete account creation'

!include bitbucket_example.md screenshot '02' highlight 'Push demo.+' caption 'Create a repository for the demo'

!include bitbucket_example.md screenshot '03' highlight 'Push demo.+' caption 'Create a repository for the demo'

!include bitbucket_example.md screenshot '05' highlight 'Push demo.+' caption 'Create a repository for the demo'

!include bitbucket_example.md screenshot '06' highlight 'Push demo.+' caption 'Since we have an existing repository to upload, we follow the instructions for *Get your local repository on BitBucket*'

!include bitbucket_example.md screenshot '07' highlight 'Push demo.+' caption 'We go to the GIT bash to upload. We need to use the https protocol (instead of ssh) on the RCSI network'

!include bitbucket_example.md screenshot 'sslVerify' highlight 'Push demo.+' caption 'Also to use git from the RCSI network, we need a [workaround](https://confluence.atlassian.com/fishkb/unable-to-clone-git-repository-due-to-self-signed-certificate-376838977.html) for ssl verification'

!include bitbucket_example.md screenshot '09_landing_page' highlight 'Show history.+' caption 'The BitBucket landing page for the repository shows the list of files and when they were last changed'

!include bitbucket_example.md screenshot '10_commits' highlight 'Show history.+' caption 'We can see the history of commits'

!include bitbucket_example.md screenshot '11_example_commit' highlight 'Show history.+' caption 'The content of the last commit'

!include bitbucket_example.md screenshot '12_example_commit' highlight 'Show history.+' caption 'The content of the last commit'

!include bitbucket_example.md screenshot '13_annotate' highlight 'Show history.+' caption 'BitBucket has an annotate feature which highlights when each line in the file was last changed'

!include bitbucket_example.md screenshot '14_markdown' highlight 'Show history.+' caption 'Markdown rendering of the manuscript file'

!include bitbucket_example.md screenshot '15_clone' highlight 'Cloning.+' caption 'Collaborator clones repository'

!include bitbucket_example.md screenshot '16_collaborator_manuscript_change' highlight 'Making changes' caption 'Collaborator adds text on bioinformatic analysis'

!include bitbucket_example.md screenshot '17_collaborator_commit' highlight 'Making changes' caption 'Collaborator commits their change'

!include bitbucket_example.md screenshot '18_collaborator_push' highlight 'Pushing and pulling' caption 'Collaborator tries to push their change to the bitbucket server. This fails, because another change has been made after they clone'

!include bitbucket_example.md screenshot '19_collaborator_pull1' highlight 'Pushing and pulling' caption 'Collaborator needs to first pull from the server'

!include bitbucket_example.md screenshot '19_collaborator_pull2' highlight 'Pushing and pulling' caption 'The pull results in a merge between the two changes. They accept the default commit message for the merge'

!include bitbucket_example.md screenshot '19_collaborator_pull3' highlight 'Pushing and pulling' caption 'The pull is successful'

!include bitbucket_example.md screenshot '20_collaborator_push' highlight 'Pushing and pulling' caption 'Now they can push their change to the server'

!include bitbucket_example.md screenshot '21_history' highlight 'Inspecting history' caption 'The server now shows history for the file that includes both the collaborators changes and my other simultaneous change, and show that they have been merged together'

!include bitbucket_example.md screenshot '22_annotate' highlight 'Inspecting history' caption 'When we annotate the file we see the bioinformatic analysis text from the collaborator and the samples information from our change'

## Using branches and tags

::: columns

:::: {.column width="60%"}

- **Implicit branches:**
  - The collaborator has their own temporary unnamed branch when working on the files
    simultaneous to other work. The branches get merged when they pull and push.
  - Similarly you can get temporary unnamed branches when doing work on two computers
- **Explicit branches:**
  - You can create a named branch for changes that you want to keep separate from your main work (master branch)
  - The branches optionally be can be be merged later
- **Tags:**
  - You can name a specific revision (snapshot), like a **milestone**, to keep track of it
  - For example to have a record of what specific revision was used for:
    - a paper submission
    - conference talk
::::

:::: {.column width="40%"}
![<small>From <https://i2.wp.com/digitalvarys.com/wp-content/uploads/2019/06/></small>](assets/GIT-Branchand-its-Operations.png)
::::

:::

!include bitbucket_example.md screenshot '23_history_branch' highlight 'Inspecting history' caption 'The history graph shows a *figures* branch for work on the figures that is kept separate from the rest'

!include bitbucket_example.md screenshot '24_history_merge' highlight 'Inspecting history' caption 'The figures branch was merged with the rest of the work'

!include bitbucket_example.md screenshot '25_history_tag' highlight 'Inspecting history' caption 'We tagged the revision we shared with the other co-authors'

# Exercise 2

## Exercise 2 (20 min)

1. Create a BitBucket account
2. Push your "christmas_repo" from exercise 1 to BitBucket
3. Add more past_gifts and push to BitBucket
4. Update recipients through the BitBucket web-interface, and pull to your local machine
5. Collaborate in pairs

# Wrap-up

## Take home messages

- Version control with GIT helps keep your file history organized
- Light weight: minimum effort required
- If you are not comfortable with using the command line, download a GUI or use GIT from your IDE
- Commit early and often
- Write good commit messages - future you will appreciate it
- Useful for backups and for collaboration
    
## Thanks & Questions

::: columns
:::: {.column width="65%"}
- Get int touch: <manuelasalvucci@rcsi.ie>

- Presentation, CheatSheet, Handout and Solution: <https://bitbucket.org/manuela_s/git_workshop/downloads/>
- Workshop repo: <https://bitbucket.org/manuela_s/git_workshop>

- Useful resources
    - <https://git-scm.com/>
    - <https://git-scm.com/book/en/v2>
    - <https://try.github.io/>
    - <https://stackoverflow.com/questions/tagged/git>
    - <https://sethrobertson.github.io/GitBestPractices/>
::::

:::: {.column width="35%"}
![<small>From <https://raw.githubusercontent.com/hendrixroa/in-case-of-fire-1/master/></small>](assets/in_case_of_fire.png)
::::

:::