# Resize virtual machine window for screenshots
```bash
xdotool search --name "virtual_windows" windowsize 1165 727
```

# Grab screenshot
```bash
gnome-screenshot -w -p -d 3 -f screenshot_git_bash.png
```

# Generate reveal presentation
```bash
./binder/start
```

# Generate reveal presentation in docker
```bash
repo2docker --editable .
```

# To present
To present in full-screen: Fn - F11