#!/usr/bin/env python
"""
Panflute filter to include other files, and replace keywords

Each include statement has its own line and has the syntax:

    !include ../somefolder/somefile key1 value1 key2 value2 ...

Each include statement must be in its own paragraph. That is, in its own line
and separated by blank lines.

Based on example in http://scorreia.com/software/panflute/guide.html
"""

import os
import panflute
import string
import re

def is_include_line(elem):
    if len(elem.content) < 3:
        return False
    elif not all(isinstance(x, (panflute.Str, panflute.Space, panflute.Quoted)) for x in elem.content):
        return False
    elif elem.content[0].text != '!include':
        return False
    elif type(elem.content[1]) != panflute.Space:
        return False
    else:
        return True


def get_arguments(elem):
    args = {}
    # Every 4th element starting with element 4 should be a key
    # Every 6th element starting with element 6 should be the corresponding value
    for key, value in zip(elem.content[4::4], elem.content[6::4]):
        args[panflute.stringify(key)] = panflute.stringify(value)
    return args


def action(elem, doc):

    if isinstance(elem, panflute.Para) and is_include_line(elem):
        fn = panflute.stringify(elem.content[2])

        if not os.path.isfile(fn):
            return

        args = get_arguments(elem)

        with open(fn) as f:
            raw = f.read()
            substituted = string.Template(raw).safe_substitute(args)

        if 'highlight' in args:
            substituted = re.sub('({})'.format(args['highlight']), '[\\1]{.highlighted}', substituted, count=1)
        return panflute.convert_text(substituted)


def main(doc=None):
    return panflute.run_filter(action, doc=doc)


if __name__ == '__main__':
    main()