## Practical example { .example data-transition=“fade”}

::: columns
:::: {.column width="30%"}
1. Make a project folder
2. Start a GIT bash in the project folder
3. Configure GIT
4. Initialize repository
5. Standard workflow
   1. Make edits
   2. (git add)
   3. git commit
   4. Repeat
6. git status
7. git diff
8. git log
9. git revert
::::

:::: {.column width="70%"}
![**$caption**](assets/screenshot_$screenshot.png){ .screenshot }
::::
:::
