## BitBucket example  { .example data-transition=“fade”}

::: columns
:::: {.column width="30%"}
1. Create account and log in
2. Push demo repository to BitBucket
3. Show history and diffs
4. Collaboration scenario
    1. Cloning repository (before last commit)
    2. Making changes
    3. Pushing and pulling
    4. Inspecting history
::::

:::: {.column width="70%"}
![**$caption**](assets/bitbucket_$screenshot.png){ .screenshot }
::::
:::
